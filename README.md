[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

![ObeyTheGoat](https://covers.oreillystatic.com/images/0636920051091/lrg.jpg "ObeyTDD")

techniques and learning of the methodology of programing oriented to tests or TDD following the orientations of http://www.obeythetestinggoat.com/

## Functional tests

**A functional test**, check the application from the outside, from the point of view
of the user. **The Unittest** check the application from the inside, from the point
of view of the programmer.

**Unit tests** are really about testing logic, flow control, and configuration.

![FT](static/images/ft_glosary_00.png)

## Django basic commands:

![django commands](static/images/django_basic_commands_00.png)

** From chapter six (6) and next:**

![django commands update](static/images/django_basic_commands_01.png)

## Overall TDD process

![Overall TDD Process](static/images/overall_tdd_process_00.png)

### Process with unittest & FunctionalTest:
![Overall TDD Process](static/images/overall_tdd_process_01.png)

## Interesting links:

* [support code of the book](https://github.com/hjwp/book-example)
